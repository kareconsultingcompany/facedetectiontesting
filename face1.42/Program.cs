﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace face1._42
{
    class Program
    {

        static FaceServerSdkLib.FaceServerSdkCtrl objSdk;
        static String svn;
        [STAThread]
        static void Main(string[] args)
        {
            objSdk = new FaceServerSdkLib.FaceServerSdkCtrl();
            objSdk.OnEventCConnect += objSdk_OnEventCConnect;
            objSdk.OnEventCTrap += objSdk_OnEventCTrap;
            Console.WriteLine("Started");
            objSdk.C_ServerInit(33303, 0);
            Console.WriteLine("Listening");
            Console.ReadLine();
            FaceServerSdkLib.UserInfo User = new FaceServerSdkLib.UserInfo();
            int leng = 0;
            String base64 = "";
            string strFeature = "";
            objSdk.C_GetSingleUserFeature(svn, 16, 0, ref strFeature);


            int nRet = objSdk.C_GetSingleUserInfo(svn, 16, 0, ref User);
            if (nRet == 0)
            {
                Console.WriteLine("GetSingleUserInfo" + User.UserID + ",Base64:" + User.PhotoBase64Data);
                leng = User.PhotoLen;
                base64 = User.PhotoBase64Data;
            }
            else
            {
                Console.WriteLine("C_GetSingleUserInfo failed！Errcode：" + Convert.ToString(objSdk.C_SdkGetLastError(svn), 16));
            }
            Console.ReadLine();
            long lRet = objSdk.C_DelAllUsers(svn);
            Console.WriteLine("Deleted");
            Console.ReadLine();
            /*  FaceServerSdkLib.UserInfo user = new FaceServerSdkLib.UserInfo();
              user.UserType = 0;
              user.UserID = 11;
              user.CardNo = "0000000000";
              user.IDCard = null;
              user.UserName = "test";
              user.VerifyMode = 1;
              user.Status =0;
              user.PhotoLen =leng;
              user.PhotoBase64Data = base64;
              user.RegStatus = 0;*/


            FaceServerSdkLib.UserInfoAndFeature user = new FaceServerSdkLib.UserInfoAndFeature();
            user.UserType = 0;
            user.UserID = 11;
            user.CardNo = "0000000000";
            user.IDCard = null;
            user.UserName = "test";
            user.VerifyMode = 1;
            user.Status = 0;
            user.PhotoLen = leng;
            user.PhotoBase64Data = base64;
            user.RegStatus = 0;
            user.FeatureBase64Data = strFeature;


            long lRet1 = objSdk.C_AddUserAndFeature(svn, user);
            //  long lRet1 = objSdk.C_AddUser(svn, user);
            if (lRet1 != 0)
            {
                Console.WriteLine("Error");
            }
            else
            {
                Console.WriteLine("C_AddUserAndFeature Success!");
            } Console.WriteLine("Added");
            Console.ReadLine();




        }

        static void objSdk_OnEventCTrap(string strDevSn, int lOpCode, int lUserData, int lExtendParam, int lUserID, string strRecTime, int lRecType, int lScore, int lStatus, int lPhotoType, int lPhotoLen, int lReason, string strBase64PhotoData)
        {
            Console.WriteLine("Have data");
            Console.WriteLine("" + lUserID);
        }

        static void objSdk_OnEventCConnect(string strDevSn, int lOpCode, int lUserData, int lExtendParam, string strIP, int lPort)
        {
            Console.WriteLine(strDevSn);
            svn = strDevSn;
        }
    }

}
